/**
 * @license Created by felix on 16-6-27.
 * @email   307253927@qq.com
 */
'use strict';
angular.module('app', ['ngRoute', 'ngAnimate'])
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: 'tpl-front.html'
    });
    $routeProvider.when('/detail/:video?', {
      templateUrl: 'tpl-detail.html',
      controller: 'videoCtrl'
    });
    $routeProvider.when('/result/:video?', {
      templateUrl: 'tpl-result.html',
      controller: 'appCtrl'
    });
    $routeProvider.otherwise({redirectTo: '/'});
  }])
  .run(['$rootScope', '$location', function ($rootScope, $location) {
    $rootScope.unsoeRun = function (cbObj) {
      cbObj = cbObj || {};
      cbObj.onError = function (msg) {
        alert('错误：' + msg);
      };
      unsoe.create("recordFlash", 'libs/jsSDK/', cbObj);
    };
    $rootScope.record = function (text) {
      unsoe.setContent(text);
      unsoe.start();
    };
    $rootScope.stopRecord = function () {
      unsoe.stop();
    };
    $rootScope.go = function (path) {
      $location.path(path);
    };
    var audio = $('#audio');
    $rootScope.$on('$routeChangeSuccess', function () {
      if(!audio[0].paused){
        audio[0].pause();
        audio[0].currentTime = 0;
      }
    });
  }])
  .controller('appCtrl', ['$scope', '$routeParams', '$timeout', function ($scope, $routeParams, $timeout) {
    $scope.video_c = {
      lines: [
        {who: "Baymax:", text: "Hello.I am baymax,your personal healthcare companion."},
        {who: "Hiro:", text: "Hey,baymax. I did’t know you were still active."},
        {who: "Baymax:", text: "I heard a sound of distress.What seems to be the trouble?"},
        {who: "Hiro：", text: "I just stubbed my toe a little.I’m fine."},
        {who: "Baymax:", text: "On a scale of 1 to 10, how would you rate your pain?"},
        {who: "Hiro：", text: "A zero. I’m okey,really.thanks.You can shrink now."},
        {who: "Baymax:", text: "Does it hurt when I touch it?"},
        {who: "Hiro:", text: "That’s okey.No touching.I’m fine..."}
      ],
      text: "Hello.I am baymax,your personal healthcare companion.Hey,baymax. I did’t know you were still active.I heard a sound of distress.What seems to be the trouble?I just stubbed my toe a little.I’m fine.On a scale of 1 to 10, how would you rate your pain?A zero. I’m okey,really.thanks.You can shrink now.Does it hurt when I touch it?That’s okey.No touching.I’m fine..."
    };
    $scope.video_f = {
      lines: [
        {who: "Judy Hopps:", text: "Polar bear fur."},
        {who: "Nick Wilde:", text: "Oh,my god!"},
        {who: "Judy Hopps:", text: "What? What?"},
        {
          who: "Nick Wilde:",
          text: "The Velvety Pipes of Jerry Vole. But on CD .Who still uses CDs!   Carrots,if your otter was here,he had a very bad day."
        },
        {who: "Judy Hopps:", text: "Those are claw marks. You ever anything like this?"},
        {who: "Nick Wilde:", text: "No"},
        {who: "Judy Hopps:", text: "Oh. Wait.  Look!"}
      ],
      text: "Oh,my god! What? What? The Velvety Pipes of Jerry Vole. But on CD .Who still uses CDs!   Carrots,if your otter was here,he had a very bad day.Those are claw marks. You ever anything like this? No Oh. Wait.  Look!"
    };
    $scope.video_t = {
      lines: [
        {who: "Rose:", text: "They said you might be up here..."},
        {
          who: "Jack:",
          text: "Give me your hand. Now close your eyes. Go on. Now step up. Now hold on the railing. Keep your eyes closed. Don’t peek."
        },
        {who: "Rose:", text: "I’m not."},
        {who: "Jack:", text: "Step up onto the rail. Hold on. Hold on.Keep your eyes closed. Do you trust me?"},
        {who: "Rose:", text: "I trust you."}
      ],
      text: "They said you might be up here...Give me your hand. Now close your eyes. Go on. Now step up. Now hold on the railing. Keep your eyes closed. Don’t peek.I’m not.Step up onto the rail. Hold on. Hold on.Keep your eyes closed. Do you trust me?I trust you."
    };
    $scope.video = $scope[$routeParams.video];
    $scope.video_play = $('#videoPlay');
    $scope.video_play.attr('poster',"images/"+$routeParams.video+".png");
    $scope.videoUrl = "videos/" + $routeParams.video + ".mp4";
    var audio = $('#audio'), timer;
    $scope.uns = {
      onSpeechBegin: function () {
        $scope.video_play[0].muted = true;
        $scope.video_play[0].play();
        $timeout.cancel(timer);
        timer = $timeout(function () {
          $scope.video.end = true;
          $scope.video.isPlaying = false;
          $scope.stopRecord();
        }, 70000);
      },
      onSpeechEnd: function () {
        $timeout.cancel(timer);
      },
      onResult: function (url, json) {
        $scope.$apply(function () {
          $scope.wordList = json.lines[0].words;
          $scope.score = Math.floor(json.lines[0].score);
          audio[0].src = 'videos/' + ($scope.score>=60?'right.mp3':'error.mp3');
          audio[0].play();
        });
      }
    };
    $scope.unsoeRun($scope.uns);
    $scope.times = 3;
    (function run() {
      $timeout(function () {
        if(--$scope.times == 0){
          $scope.record($scope.video.text);
          $scope.video.isPlaying = true;
        } else {
          run();
        }
      }, 1000);
    })();
    $scope.stop = function () {
      $scope.video_play[0].pause();
      $scope.stopRecord();
      $scope.video.end = true;
      $scope.video.isPlaying = false;
    };
  }])
  .controller('videoCtrl', ['$scope', '$routeParams','$timeout', function ($scope, $routeParams,$timeout) {
    $scope.params = $routeParams;
    $scope.video = $('#video_play');
    $scope.video.attr('poster',"images/"+$routeParams.video+".png");
    $scope.videoUrl = "videos/" + $routeParams.video + ".mp4";
    $scope.play = function () {
      $scope.video[0].play();
      $scope.video.isPlaying = true;
    };
    $scope.stop = function () {
      if($scope.video.isPlaying){
        $scope.video[0].pause();
        $scope.video.isPlaying = false;
        $scope.video[0].currentTime = 0;
      }
    };
    $scope.video[0].onended = function () {
      $timeout(function () {
        $scope.video.isPlaying = false;
        $scope.video[0].currentTime = 0;
      });
    }
  }])
  
  
;