var gulp = require('gulp')
var scss = require('gulp-scss')

gulp.task('scss',function () {
  gulp.src('scss/*.scss')
    .pipe(scss())
    .pipe(gulp.dest('css'));
});
gulp.task('default',['scss'],function () {
  gulp.watch('scss/*.scss',['scss']);
});